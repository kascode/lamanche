'use strict';
module.exports = function(sequelize, DataTypes) {
  var Point = sequelize.define('Point', {
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Point;
};
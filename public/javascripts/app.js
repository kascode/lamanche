/**
 * Created by mailf on 22.06.2016.
 */

var points = [];

document.addEventListener('DOMContentLoaded', function () {
    // console.log("DOM locaded");
    // var positionWatcher = navigator.geolocation.watchPosition(function (position) {
    //     console.log("Position changed", position);
    //     fetch('/points/' + position.coords.latitude + ',' + position.coords.longitude, {
    //         method: 'POST'
    //     }, function (err) {
    //         console.log(err);
    //     }, {
    //         enableHighAccuracy: true,
    //         timeout: 1000 * 60
    //     });
    // });
    //
    // setInterval(function () {
    //     navigator.geolocation.getCurrentPosition(function (position) {
    //         console.log("Position got", position);
    //         fetch('/points/' + position.coords.latitude + ',' + position.coords.longitude, {
    //             method: 'POST'
    //         }); 
    //     }, function (err) {
    //         console.log(err);
    //     }, {
    //         enableHighAccuracy: true,
    //         timeout: 1000 * 60
    //     });
    // }, 1000);

    getHistory()
        .then(function (res) {
            return res.json();
        })
        .then(function (ps) {
            points = ps;
            drawHistory(points);
        });
});

function getHistory () {
    return fetch('/points');
}

function drawHistory(points) {
    var swimTime = new Date(points[points.length-1].createdAt).getTime() - new Date(points[0].createdAt).getTime();
    var degToDraw = 360 * (swimTime / (1000*60*180));
    var deg = 0;
    var pointer = document.querySelector('.p1');
    var canvas = document.querySelector('.path');
    var ctx = canvas.getContext('2d');
    var timePassed = 0;
    var degSpeed = 9 / 60 / 180;
    ctx.lineJoin = "round";
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.moveTo(686, 370);

    degToDraw = 360;

    console.log("Swim time:", swimTime, degToDraw);

    rotateElOnDegT(pointer, 0, timePassed);

    var int = setInterval(function () {
        // rotateElOnDeg(document.querySelector('.p1'), deg, Math.random() * 1.4);

        console.log(timePassed/1000/60, deg);
        rotateElOnDegT(document.querySelector('.p1'), deg, timePassed);

        ctx.lineTo(pointer.offsetLeft + 30, pointer.offsetTop + 30);
        ctx.stroke();

        timePassed += 120000;
        deg += 1;

        if (deg > degToDraw)
            clearInterval(int);
    }, 100);
}

function rotateElOnDegT(el, deg, time) {
    // var d = 300 - (60 * (time / (1000*60*60)));
    var d = 315 - (60 * time / (1000*60*180));
    // var d = 315;
    var x = Math.cos(deg/90) * d + 340;
    var y = Math.sin(deg/90) * d + 340;
    el.style.left = x + 'px';
    el.style.top = y + 'px';
}
/**
 * Created by mailf on 22.06.2016.
 */
var express = require('express');
var router = express.Router();
var models = require('../models');

router.get('/', function(req, res) {
    models.Point.findAll({
        attributes: ['id', 'latitude', 'longitude', 'createdAt'],
        order: 'createdAt ASC'
    })
        .then(function (points) {
            res.send(points);
        })
});

router.get('/last', function(req, res) {
   models.Point.findAll({
       attributes: ['id', 'latitude', 'longitude', 'createdAt'],
       order: 'createdAt ASC'
   })
       .then(function (points) {
           res.send(points[points.length-1]);
       })
});

/* POST point listing. */
router.post('/:lat,:lng', function(req, res, next) {
    var lat = parseFloat(req.params.lat);
    var lng = parseFloat(req.params.lng);
    
    models.Point.create({
        latitude: lat,
        longitude: lng
    })
        .then(function(p) {
            res.send(p);
        });
});

module.exports = router;